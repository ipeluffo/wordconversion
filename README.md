# Word Conversion Coding Challenge for Ink Global

## Create JAR for library
./gradlew clean jar

The library can be found in the following path: `build/libs/wordconversion-1.0-SNAPSHOT.jar`

## Run tests
./gradlew clean test

## Run sample conversions
./gradlew clean run