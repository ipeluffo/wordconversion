package ipeluffo.wordconversion;

/**
 * Created by ipeluffo on 7/7/15.
 */
public class NumberToStringConverter {

    private String[] numbers = { "", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
            "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen",
            "nineteen"};

    private String[] tens = {"", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty",
            "ninety" };

    private String[] iterations = {"", " thousand ", " million ", " billion "};


    public String convertNumberInWords(int number) {
        if (number == 0) {
            return "zero";
        }

        String result = "";
        int tempNumber = Math.abs(number);
        int lessThanThousand;

        for (int i = 0; tempNumber > 0; i++) {
            lessThanThousand = tempNumber % 1000;
            result = lessThanThousandInWords(lessThanThousand) + iterations[i] + result;
            tempNumber = tempNumber / 1000;
        }

        if (number < 0) {
            result = "minus " + result;
        }

        return result;
    }


    private String lessThanThousandInWords(int number) {
        if (number == 0) {
            return "";
        }

        StringBuilder result = new StringBuilder();
        int hundred = number / 100;
        int mod = number % 100;

        if (hundred > 0) {
            if (mod > 0)
                result.append(numbers[hundred]).append(" hundred and ");
            else
                result.append(numbers[hundred]).append(" hundred");
        }

        if (mod > 19) {
            result.append(tens[mod / 10]).append(" ").append(numbers[mod % 10]);
        } else {
            result.append(numbers[mod]);
        }

        return result.toString();
    }

}
