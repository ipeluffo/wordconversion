package ipeluffo.wordconversion;

/**
 * Created by ignacio.peluffo on 7/7/2015.
 */
public class WordConversionMain {

    public static void main(String[] args) {
        NumberToStringConverter converter = new NumberToStringConverter();

        System.out.println(0 + " = " + converter.convertNumberInWords(0));
        System.out.println(1 + " = " + converter.convertNumberInWords(1));
        System.out.println(21 + " = " + converter.convertNumberInWords(21));
        System.out.println(105 + " = " + converter.convertNumberInWords(105));
        System.out.println(600 + " = " + converter.convertNumberInWords(600));
        System.out.println(56945781 + " = " + converter.convertNumberInWords(56945781));
        System.out.println(999999999 + " = " + converter.convertNumberInWords(999999999));
        System.out.println(-123 + " = " + converter.convertNumberInWords(-123));
        System.out.println(600000 + " = " + converter.convertNumberInWords(600000));
        System.out.println(Integer.MAX_VALUE + " = " + converter.convertNumberInWords(Integer.MAX_VALUE));
    }

}
