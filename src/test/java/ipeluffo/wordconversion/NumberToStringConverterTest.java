package ipeluffo.wordconversion;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by ipeluffo on 7/7/15.
 */
public class NumberToStringConverterTest {

    NumberToStringConverter converter = new NumberToStringConverter();

    @Test
    public void testZero() {
        String result = converter.convertNumberInWords(0);
        Assert.assertEquals("zero", result);
    }

    @Test
    public void testMinusZero() {
        String result = converter.convertNumberInWords(-0);
        Assert.assertEquals("zero", result);
    }

    @Test
    public void testOne() {
        String result = converter.convertNumberInWords(1);
        Assert.assertEquals("one", result);
    }

    @Test
    public void test21() {
        String result = converter.convertNumberInWords(21);
        Assert.assertEquals("twenty one", result);
    }

    @Test
    public void test105() {
        String result = converter.convertNumberInWords(105);
        Assert.assertEquals("one hundred and five", result);
    }

    @Test
    public void test56945781() {
        String result = converter.convertNumberInWords(56945781);
        Assert.assertEquals("fifty six million nine hundred and forty five thousand seven hundred and eighty one", result);
    }

    @Test
    public void testNegative() {
        String result = converter.convertNumberInWords(-123);
        Assert.assertEquals("minus one hundred and twenty three", result);
    }

    @Test
    public void testMaxInt() {
        String result = converter.convertNumberInWords(Integer.MAX_VALUE);
        Assert.assertEquals("two billion one hundred and forty seven million four hundred and eighty three thousand six hundred and forty seven",
                result);
    }

}
